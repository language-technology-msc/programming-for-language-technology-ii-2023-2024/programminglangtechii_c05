import nltk

# there are other distance measures

# Set X, Set Y
# J(X,Y) = |X ∩ Y| / | X ∪ Y|
# Number of items both in X and Y / Number of distinct items that belong to X or Y

# Jaccard distance
# D(X,Y) = 1 – J(X,Y)

set_X = set('mapping')
set_Y = set('mappings')

jaccard = nltk.jaccard_distance(set_X, set_Y)
print(jaccard)

# why
print(set_X)
print(set_Y)
# {'g', 'a', 'm', 'i', 'n', 'p'}
# {'g', 'a', 'm', 'i', 'n', 'p', 's'}
# |X ∩ Y| = {'g', 'a', 'm', 'i', 'n', 'p'} -> 6 items
# |X ∪ Y| = {'g', 'a', 'm', 'i', 'n', 'p', 's'} -> 7 items
# D(X,Y) = 1 – 6/7 =  0.142857..

print()
# -------------------------------------------
# we can also measure distance in ngram level
# -------------------------------------------

# examples taken from https://examples.yourdictionary.com/paraphrase-examples-rewording-sentences.html
tokens1 = nltk.word_tokenize('Niagara Falls is viewed by thousands of tourists every year.')
tokens2 = nltk.word_tokenize('Each year, thousands of people visit Niagara Falls.')

# extract tokens
for n in [1, 2, 3]:
    print("\nn=" + str(n))
    ngrams1 = set(nltk.ngrams(tokens1, n))
    ngrams2 = set(nltk.ngrams(tokens2, n))
    print(ngrams1)
    print(ngrams2)
    print(nltk.jaccard_distance(ngrams1, ngrams2))


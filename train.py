# import nltk as always
import nltk

# import abc corpus
# from the corpora that are included in nltk.corpus

# if you haven't downloaded all nltk
# corpora or abc then
# nltk.download('abc')
from nltk.corpus import abc

# word2vec is implemented in a different package (not in NLTK)
import gensim

# train a model
model = gensim.models.Word2Vec(sentences=abc.sents(), vector_size=100, window=5, min_count=1, workers=4)

model_path ="./model/word2vec.model"
print('saving ' + model_path)
model.save(model_path)
print('saved ' + model_path)


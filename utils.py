from gensim.models import Word2Vec

def print_similar_words(model, word):
    print(word)
    print("----------------------")

    if model.get_index(word, -1) >= 0:
        # Get words with top similarities
        # ...
        # This method computes cosine similarity between a simple mean of the projection
        # weight vectors of the given keys and the vectors for each key in the model
        sims = model.most_similar(word, topn=10)

        # print similar words
        for sim in sims:
            print(sim)
    else:
        print(word + " not found")
        print("----------------------")



import gensim.downloader
from gensim.models import Word2Vec
from utils import print_similar_words

# Show all available models in gensim-data
print(list(gensim.downloader.info()['models'].keys()))

# load a model
model_name = 'word2vec-google-news-300'
# Download (if needed) dataset/model and load it to memory
model = gensim.downloader.load(model_name)

# [=-------------------------------------------------] 2.9% 49.0/1662.8MB downloaded

# ['fasttext-wiki-news-subwords-300', 'conceptnet-numberbatch-17-06-300', 'word2vec-ruscorpora-300', 'word2vec-google-news-300', 'glove-wiki-gigaword-50', 'glove-wiki-gigaword-100', 'glove-wiki-gigaword-200', 'glove-wiki-gigaword-300', 'glove-twitter-25', 'glove-twitter-50', 'glove-twitter-100', 'glove-twitter-200', '__testing_word2vec-matrix-synopsis']
# [==================================================] 100.0% 1662.8/1662.8MB downloaded
# loaded word2vec-google-news-300

print('loaded ' + model_name)

print_similar_words(model, 'computer')
print_similar_words(model, 'robot')
print_similar_words(model, 'coronovarius')

'''
computer
----------------------
('computers', 0.7979379892349243)
('laptop', 0.6640493273735046)
('laptop_computer', 0.6548868417739868)
('Computer', 0.647333562374115)
('com_puter', 0.6082080006599426)
('technician_Leonard_Luchko', 0.5662748217582703)
('mainframes_minicomputers', 0.5617720484733582)
('laptop_computers', 0.5585449934005737)
('PC', 0.5539618730545044)
('maker_Dell_DELL.O', 0.5519254207611084)
robot
----------------------
('robots', 0.8341808319091797)
('robotic', 0.8093317151069641)
('Robot', 0.6990479230880737)
('humanoid', 0.6654586791992188)
('robotics', 0.6461701393127441)
('humanoid_robots', 0.6394188404083252)
('Honda_Asimo', 0.6256935000419617)
('autonomous_robots', 0.6215381026268005)
('GeckoSystems_suite', 0.6206844449043274)
('i_SOBOT', 0.6200507283210754)
coronovarius
----------------------
coronovarius not found
----------------------
'''